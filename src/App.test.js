import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { mount, shallow, configure } from 'enzyme';
import renderer from 'react-test-renderer';

import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Initial tests', () => {
	it('renders without crashing', () => {
  		shallow(<App />);
	});
	it('sum numbers', () =>{
		expect(2 + 2).toBe(4);
	});
});

describe('Persons', () => {
	const app = mount(<App />);
	it('create worker', () => {
		app.find('#nameInput').value = 'Павел';
		app.find('Button#buttonAdd').simulate('click');
		console.log(app.find('ControlPanel'));
		//expect(app.find('ListGroupItem').length).toBe(1);
	});
});